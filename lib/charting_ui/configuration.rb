module ChartingUi
	class Configuration
		attr_accessor :default_title_tag, :default_font_size, :default_font_family, :default_offset_left, :default_offset_right, :default_offset_top, :default_offset_bottom

		def initialize
			@default_title_tag = "h3"
			@default_font_size = 12
			@default_font_family = "Arial"
			@default_offset_left = 50
			@default_offset_right = 20
			@default_offset_top = 30
			@default_offset_bottom = 40
		end
	end
end
