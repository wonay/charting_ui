module ChartingUi
	class Engine < ::Rails::Engine
		isolate_namespace ChartingUi

		initializer "charting_ui", before: :load_config_initializers do |app|
		Rails.application.routes.append do
			mount ChartingUi::Engine, at: "/charting_ui"
		end
	end
	end
end
