#= require ./charting_core

class window.ChartEngine_Histogram extends ChartEngine_Core

	offset_bar: 5
	alone: false
	min_height: 3
	is_dates: false

	#########################################
	############### SETUP ###################
	constructor: (block) ->
		super(block)

		$(@info_box).css('border-radius', '0px')
		if @alone
			$(@info_box).css('opacity', '1')
		else
			$(@info_box).css('opacity', '0.8')

		if @is_dates
			@create_extra_menu($(block).parent())

		console.log "Chart histogram initialized!"

	create_extra_menu: (container) ->
		self = this
		block = '<div class="charting_ui_date_range_pick">'
		block += '<span>Date range: </span>'
		block += '<select class="charting_ui_date_range_selector">'
		block += '<option value="all" selected="selected">All</option>'
		block += '<option value="day">per Days</option>'
		block += '<option value="month">per Months</option>'
		block += '<option value="year">per Years</option>'
		block += '</select>'
		block += '</div>'

		container.append($(block))

		$(container).on('change','.charting_ui_date_range_selector', (event) ->
			self.load_source($(this).val())
			self.redraw()
		)

		return block

	get_info_box_content: () ->
		return '<h3 class="charting_ui_info_main_title"></h3><ul class="charting_ui_info_container"></ul>'

	create_info_info_box: (index, main_title, titles) ->
		container = $(@info_box).find('ul.charting_ui_info_container')

		block = '<li>'
		if ! @alone
			block += '<span class="charting_ui_info_title charting_ui_info_title_' + main_title + '" style="color:' + @colors[index] + '" >' + main_title + ':</span>&nbsp;'
		block += '<ul style="display:inline-block;">'
		$.each(titles, (i, t) ->
		       block += '<li style="display:inline-block;"><span class="charting_ui_info_sub_title charting_ui_info_sub_title_' + main_title + '_' + t.class_name + '" >' + t.name + ':</span>&nbsp;<span class="charting_ui_info_value charting_ui_info_box_value_' + main_title + '_' + t.class_name + '"></span>&nbsp;</li>'
		)
		block += '</ul></li>'
		container.append($(block))


	load_source: (date_scope = "all") ->
		model = @data.model.name
		key = @data.model.key
		self = this
		success = false

		func_done = (data) ->
			self.source = data.success
			self.min_x = 0
			self.max_x = self.source.length
			self.min_y = 0
			self.max_y = - Number.MAX_VALUE
			total_value = {}
			self.alone = self.source[0].values.length == 1

			$.map(self.source, (v, k) ->
				$.map(v.values, (v, k) ->
					if k of total_value
						total_value[k] += v.value
					else
						total_value[k] = v.value
				)
			)

			$.map(self.source,(v, k) ->
				type = v.key_type
				if type == "date"
					v.key = new Date(v.key)
					self.is_dates = true

				$.map(v.values, (v, k) ->
					y = parseInt(v.value)
					v.percent = self.round_up(v.value / total_value[k] * 1.0 * 100, 1)
					if y > self.max_y
						self.max_y = y
				)
			)

			$(self.info_box).find('ul.charting_ui_info_container').empty()
			$.map(self.source[0].values, (v, k) ->
				self.create_info_info_box(k, v.serie, [{name: "Sum", class_name:"sum"}, {name: "%", class_name: "percent"}])
			)

			success = true

		func_error = (data) ->
			success = false
			response = $.parseJSON data.responseText
			console.log "ERROR: #" + data.status + ": " + response.error

		func_always = (data) ->
			console.log "Data fetched"

		$.ajax({
			url: '/charting_ui/fetch_data',
			data: 'model_name=' + model + '&model_key=' + JSON.stringify(key) + "&date_scope=" + date_scope,
			async: false,
			method: 'post',
			success: func_done,
			done: func_done,
			error: func_error,
			fail: func_error,
			complete: func_always,
			always: func_always
			})

		console.log("MinX: " + @min_x + ", MaxX: " + @max_x)
		console.log("MinY: " + @min_y + ", MaxY: " + @max_y)
		return success


	before_redraw: () ->

	#########################################
	############ DRAW GLOBAL ################
	getIndentation: () ->
		return @source.length

	draw_chart: () ->
		self = this

		$.each(@source, (i, p) ->
			series = p.values.length
			$.map(p.values, (v, index) ->
				x_start =  i + (1.0 / series * index)
				x_end =  i + (1.0 / series * (index + 1))
				top_left = new Chart_Point(x_start, v.value)
				bottom_right = new Chart_Point(x_end, self.min_y)
				top_left = self.convert_unit_to_pixel(top_left)
				if v.value == 0
					top_left.y -= self.min_height

				bottom_right = self.convert_unit_to_pixel(bottom_right)
				if index == 0
					top_left.x += self.offset_bar
				if index == series - 1
					bottom_right.x -= self.offset_bar


				self.draw_rectangle(top_left, bottom_right, self.colors[index], "white", 0)
			)
		)

	get_axe_text_x_pixel_position: (n, indent) ->
		return @convert_unit_to_pixel_x(n + @indent_x / 2)

	get_axe_text_x_text: (n) ->
		if n >= @source.length
			return null
		return @source[n].key

	#########################################
	######### UPDATE INTERACTIVE ############
	update_user_interaction: (mouse_coordinate) ->
		value_x = Math.ceil(@convert_pixel_to_unit_x(mouse_coordinate.x)) - 1
		if value_x >= @source.length
			return false
		column = @source[value_x]

		series = column.values.length
		if series == 1
			coordinate_x = value_x
			coordinate_y = column.values[0].value

			if mouse_coordinate.y < @convert_unit_to_pixel_y(coordinate_y) && coordinate_y > 0
				return false

			top_left = new Chart_Point(coordinate_x, coordinate_y)
			bottom_right = new Chart_Point(coordinate_x + 1, @min_y)

			top_left = @convert_unit_to_pixel(top_left)

			if coordinate_y == 0
				thickness = 1
				top_left.y -= @min_height
			else
				thickness = 4

			top_left.x += @offset_bar

			bottom_right = @convert_unit_to_pixel(bottom_right)
			bottom_right.x -= @offset_bar
			bottom_right.y -= thickness / 2



			@previous_drawn_region.push(@draw_rectangle_interactive(top_left, bottom_right, @default_cross_color, "black", thickness))
		else
			slice = 1.0 / series

		return true

	updateInfoBoxPosition: (mouse_coordinate) ->
		value_x = Math.ceil(@convert_pixel_to_unit_x(mouse_coordinate.x)) - 1
		column = @source[value_x]

		coordinate_x = @convert_unit_to_pixel_x(value_x) + 1

		if @alone
			if column.values[0].value == 0
				coordinate_y = @convert_unit_to_pixel_y(@max_y)
				coordinate_x -= 2
			else
				coordinate_y = @convert_unit_to_pixel_y(column.values[0].value) + 1
		else
			coordinate_y = @convert_unit_to_pixel_y(@max_y)

		if $(@info_box).outerWidth() > @canvas_width - coordinate_x - @offset_bar
			coordinate_x = @convert_unit_to_pixel_x(value_x + 1) - 1
			@info_box.css('left', (coordinate_x - $(@info_box).outerWidth() - @offset_bar) + 'px')
		else
			@info_box.css('left', (coordinate_x + @offset_bar) + 'px')

		@info_box.css('top', (coordinate_y) + 'px')


	update_info_box_content: (mouse_coordinate) ->
		value_x = Math.ceil(@convert_pixel_to_unit_x(mouse_coordinate.x)) - 1
		column = @source[value_x]
		self = this
		@info_box.find('.charting_ui_info_main_title').text(column.key)

		$.map(column.values, (v, k) ->
			serie = v.serie
			self.info_box.find('.charting_ui_info_box_value_' + serie + '_sum').text(v.value)
			self.info_box.find('.charting_ui_info_box_value_' + serie + '_percent').text(v.percent)
		)
		#@info_box.find('.charting_ui_info_box_y').text(column.values[0].value)
		#@info_box.find('.charting_ui_info_box_extra').text(column.values[0].percent)

	#########################################
