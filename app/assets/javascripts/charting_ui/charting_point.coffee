class window.Chart_Point

	constructor: (_x, _y) ->
		@x = parseFloat(_x)
		@y = parseFloat(_y)

	to_s: () ->
		return "(" + @x + ", " + @y + ")"
