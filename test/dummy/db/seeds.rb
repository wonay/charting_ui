FactoryGirl.define do
	factory :user_custom, class: User do
		name { Forgery::Internet.user_name }
		email { Forgery::Internet.email_address }
		points { Forgery::Basic.number() }
		last_connection { Forgery::Date.date }
		setting_1 { Forgery::Basic.number(:at_least => 1, :at_most => 4) }
		setting_2 { Forgery::Basic.number(:at_least => 4, :at_most => 10) }
		setting_3 { Forgery::Personal.gender }
  	end
end

200.times { FactoryGirl.create(:user_custom) }
