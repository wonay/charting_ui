# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'charting_ui/version'

Gem::Specification.new do |spec|
  spec.name          = "charting_ui"
  spec.version       = ChartingUi::VERSION
  spec.authors       = ["Leo Benkel"]
  spec.email         = ["leo.benkel@gmail.com"]

  spec.summary       = "Will draw chart"
  spec.description   = "Will draw chart"
  spec.homepage      = "https://gitlab.com/wonay/charting_ui"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "byebug", ">= 9.0.4"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "factory_girl_rails"
  spec.add_development_dependency "forgery"

  spec.required_ruby_version = ">= 2.3.0"
  spec.add_dependency "rails", ">= 4.2.4"
  spec.add_dependency 'jquery-rails', '> 3.0.0'
end


# Useful links
# http://flowingdata.com/2010/01/07/11-ways-to-visualize-changes-over-time-a-guide/
